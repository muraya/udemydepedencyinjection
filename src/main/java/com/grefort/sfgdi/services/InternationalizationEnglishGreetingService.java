package com.grefort.sfgdi.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"EN","default"})
@Service("InternationalService")
public class InternationalizationEnglishGreetingService implements GreetingService {

    @Override
    public String sayHello() {
        return "Sending greetings in English";
    }
}
