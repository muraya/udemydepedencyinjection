package com.grefort.sfgdi.services;

public interface GreetingService {

    String sayHello();

}
