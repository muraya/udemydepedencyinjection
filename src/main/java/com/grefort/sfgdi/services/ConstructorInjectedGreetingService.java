package com.grefort.sfgdi.services;

import org.springframework.stereotype.Service;

@Service
public class ConstructorInjectedGreetingService implements GreetingService {
    @Override
    public String sayHello() {
        return "I have greeted you Constructor";
    }
}
