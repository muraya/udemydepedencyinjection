package com.grefort.sfgdi.services;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("ES")
@Service("InternationalService")
public class InternationalizationSpanishGreetingService implements GreetingService {

    @Override
    public String sayHello() {
        return "Enviando saludos en español";
    }
}
