package com.grefort.sfgdi.controllers;

import com.grefort.sfgdi.services.GreetingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class InternationalizationController {

    private final GreetingService greetingService;

    public InternationalizationController(@Qualifier("InternationalService") GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String sayHello()
    {
        return greetingService.sayHello();
    }

}
