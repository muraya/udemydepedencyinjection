package com.grefort.sfgdi.controllers;

import com.grefort.sfgdi.services.GreetingService;
import org.springframework.stereotype.Controller;

@Controller
public class MyController {

    public GreetingService greetingService;

    public MyController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public  String helloFunction()
    {
        return greetingService.sayHello();
    }

}
