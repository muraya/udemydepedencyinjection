package com.grefort.sfgdi;

import com.grefort.sfgdi.controllers.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SfgDiApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SfgDiApplication.class, args);

        System.out.println("____________Internationalization");
        InternationalizationController internationalizationController = (InternationalizationController) context.getBean("internationalizationController");
        System.out.println(internationalizationController.sayHello());

        System.out.println("________________Primary Bean");
        MyController myController = (MyController) context.getBean("myController");

        System.out.println(myController.helloFunction());

        System.out.println("________________Property");
        PropertyInjectedController propertyInjectedController = (PropertyInjectedController) context.getBean("propertyInjectedController");
        System.out.println(propertyInjectedController.getGreeting());

        System.out.println("__________________Setter");
        SetterInjectedController setterInjectedController = (SetterInjectedController) context.getBean("setterInjectedController");
        System.out.println(setterInjectedController.getGreetings());

        System.out.println("_________________Constructor");
        ConstructorInjectedController constructorInjectedController = (ConstructorInjectedController) context.getBean("constructorInjectedController");
        System.out.println(constructorInjectedController.getGreetings());



    }

}
