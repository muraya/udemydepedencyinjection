package com.grefort.sfgdi.controllers;

import com.grefort.sfgdi.services.ConstructorInjectedGreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SetterInjectedControllerTest {

    SetterInjectedController setterInjectedController;

    @BeforeEach
    void setUp() {
        setterInjectedController = new SetterInjectedController();

        setterInjectedController.greetingService = new ConstructorInjectedGreetingService();
    }

    @Test
    void setGreetingService() {
        System.out.println(setterInjectedController.getGreetings());
    }
}